package br.com.pontoeletronico.controllers;

import br.com.pontoeletronico.enums.TipoBatidaEnum;
import br.com.pontoeletronico.models.Ponto;
import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.models.dtos.PontoSaidaDTO;
import br.com.pontoeletronico.models.dtos.UsuarioEntradaDTO;
import br.com.pontoeletronico.services.PontoService;
import br.com.pontoeletronico.services.UsuarioService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.TreeMap;

@WebMvcTest(UsuarioController.class)
public class UsuarioControllerTest {

    @MockBean
    private UsuarioService usuarioService;

    @MockBean
    private PontoService pontoService;

    @Autowired
    private MockMvc mockMvc;

    Usuario usuario;
    Ponto ponto;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setNome_Completo("Alan");
        usuario.setCpf("22893285880");
        usuario.setEmail("alan@alan.com");
        ponto = new Ponto();
    }

    @Test
    public void testarCriarUsuario() throws Exception {
        Mockito.when(usuarioService.criarUsuario(Mockito.any(UsuarioEntradaDTO.class)))
                .then(usuarioObjeto -> {
                    usuario.setId(1);
                    usuario.setData_Cadastro(LocalDate.now());
                    return usuario;
                });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDeUsuario = mapper.writeValueAsString(usuario);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuario")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDeUsuario))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.data_Cadastro",
                        CoreMatchers.equalTo(LocalDate.now().toString())));
    }

    @Test
    public void testarBaterPonto() throws Exception {
        Mockito.when(pontoService.baterPonto(Mockito.anyInt(), Mockito.any(TipoBatidaEnum.class)))
                .then(pontoObjeto -> {
                    ponto.setId(1);
                    ponto.setUsuario(usuario);
                    ponto.setData_Hora_Batida(LocalDateTime.now());
                    ponto.setTipo_Batida(TipoBatidaEnum.ENTRADA);
                    return ponto;
                });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePonto = mapper.writeValueAsString(ponto);

        mockMvc.perform(MockMvcRequestBuilders.post("/usuario/1/ponto/ENTRADA")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto))
                .andExpect(MockMvcResultMatchers.status().isCreated())
                .andExpect(MockMvcResultMatchers.jsonPath("$.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.tipo_Batida",
                        CoreMatchers.equalTo(TipoBatidaEnum.ENTRADA.toString())));
    }

    @Test
    public void consultarPontosBatidos() throws Exception {
        Mockito.when(pontoService.consultarPontosBatidos(Mockito.anyInt()))
                .then(pontoObjeto -> {
                    PontoSaidaDTO pontoSaidaDTO = new PontoSaidaDTO();
                    ponto.setId(1);
                    ponto.setUsuario(usuario);
                    ponto.setData_Hora_Batida(LocalDateTime.now());
                    ponto.setTipo_Batida(TipoBatidaEnum.ENTRADA);
                    usuario.setId(1);
                    usuario.setData_Cadastro(LocalDate.now());
                    pontoSaidaDTO.setUsuario(usuario);
                    TreeMap<LocalDateTime, TipoBatidaEnum> pontos = new TreeMap<>();
                    pontos.put(ponto.getData_Hora_Batida(), ponto.getTipo_Batida());
                    pontoSaidaDTO.setPontos(pontos);
                    pontoSaidaDTO.setHorasTrabalhadas(LocalTime.of(1, 0));
                    return pontoSaidaDTO;
                });

        ObjectMapper mapper = new ObjectMapper();
        String jsonDePonto = mapper.writeValueAsString(ponto);

        mockMvc.perform(MockMvcRequestBuilders.get("/usuario/1")
                .contentType(MediaType.APPLICATION_JSON)
                .content(jsonDePonto))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.usuario.id", CoreMatchers.equalTo(1)))
                .andExpect(MockMvcResultMatchers.jsonPath("$.pontos",
                        CoreMatchers.hasItems()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.horasTrabalhadas",
                        CoreMatchers.notNullValue()));
    }
}

