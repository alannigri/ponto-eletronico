package br.com.pontoeletronico.services;

import br.com.pontoeletronico.enums.TipoBatidaEnum;
import br.com.pontoeletronico.models.Ponto;
import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.models.dtos.PontoSaidaDTO;
import br.com.pontoeletronico.repositories.PontoRepository;
import br.com.pontoeletronico.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@SpringBootTest
public class PontoServiceTest {

    @Autowired
    private PontoService pontoService;

    @MockBean
    private PontoRepository pontoRepository;

    @MockBean
    private UsuarioRepository usuarioRepository;

    Usuario usuario;
    Ponto ponto;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setCpf("22893285880");
        usuario.setNome_Completo("Alan Nigri");
        usuario.setEmail("alan@alan.com");
        usuario.setData_Cadastro(LocalDate.now());
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
    }

    @Test
    public void testarBaterPontoDeEntrada() {
        List<Ponto> pontos = new ArrayList<>();
        Mockito.when(pontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(pontos);
        Mockito.when(pontoRepository.save(Mockito.any(Ponto.class))).thenReturn(ponto);

        Ponto pontoObjeto = pontoService.baterPonto(1, TipoBatidaEnum.ENTRADA);

        Assertions.assertEquals(pontoObjeto.getTipo_Batida(), TipoBatidaEnum.ENTRADA);
        Assertions.assertEquals(pontoObjeto.getUsuario(), usuario);
    }

    @Test
    public void testarBaterPontoDeSaidaAntesDaEntrada() {
        List<Ponto> pontos = new ArrayList<>();
        Mockito.when(pontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(pontos);
        Mockito.when(pontoRepository.save(Mockito.any(Ponto.class))).thenReturn(ponto);

        Assertions.assertThrows(RuntimeException.class, () -> {
            pontoService.baterPonto(1, TipoBatidaEnum.SAIDA);
        });
    }

    @Test
    public void testarBaterPontoDeEntradaQuandoJaTemUmaEntrada() {
        ponto = new Ponto(usuario, TipoBatidaEnum.ENTRADA);
        ponto.setId(1);
        ponto.setData_Hora_Batida(LocalDateTime.now());
        List<Ponto> pontos = Arrays.asList(ponto);
        Mockito.when(pontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(pontos);
        Mockito.when(pontoRepository.save(Mockito.any(Ponto.class))).thenReturn(ponto);

        Assertions.assertThrows(RuntimeException.class, () -> {
            pontoService.baterPonto(1, TipoBatidaEnum.ENTRADA);
        });
    }

    @Test
    public void testarConsultarPontosBatidos() {
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        ponto = new Ponto(usuario, TipoBatidaEnum.ENTRADA);
        ponto.setId(1);
        ponto.setData_Hora_Batida(LocalDateTime.now());
        List<Ponto> pontos = Arrays.asList(ponto);
        Mockito.when(pontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(pontos);

        PontoSaidaDTO pontoSaidaDTO = pontoService.consultarPontosBatidos(1);

        Assertions.assertEquals(pontoSaidaDTO.getPontos().size(), 1);
        Assertions.assertEquals(pontoSaidaDTO.getPontos().get(ponto.getData_Hora_Batida()), TipoBatidaEnum.ENTRADA);
        Assertions.assertEquals(pontoSaidaDTO.getUsuario(), usuario);
    }

    @Test
    public void testarConsultarPontosBatidos2() {
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        ponto = new Ponto(usuario, TipoBatidaEnum.ENTRADA);
        ponto.setId(1);
        ponto.setData_Hora_Batida(LocalDateTime.now());
        Ponto ponto2 = new Ponto(usuario, TipoBatidaEnum.SAIDA);
        ponto2.setId(2);
        ponto2.setData_Hora_Batida(LocalDateTime.now().plusMinutes(1));
        List<Ponto> pontos = Arrays.asList(ponto, ponto2);
        Mockito.when(pontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(pontos);

        PontoSaidaDTO pontoSaidaDTO = pontoService.consultarPontosBatidos(1);

        Assertions.assertEquals(pontoSaidaDTO.getPontos().size(), 2);
        Assertions.assertEquals(pontoSaidaDTO.getPontos().get(ponto.getData_Hora_Batida()), TipoBatidaEnum.ENTRADA);
        Assertions.assertEquals(pontoSaidaDTO.getPontos().get(ponto2.getData_Hora_Batida()), TipoBatidaEnum.SAIDA);
        Assertions.assertEquals(pontoSaidaDTO.getUsuario(), usuario);
        Assertions.assertEquals(pontoSaidaDTO.getHorasTrabalhadas().getMinute(), 1);
    }

    @Test
    public void testarConsultarPontosBatidos4() {
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        ponto = new Ponto(usuario, TipoBatidaEnum.ENTRADA);
        ponto.setId(1);
        ponto.setData_Hora_Batida(LocalDateTime.now());
        Ponto ponto2 = new Ponto(usuario, TipoBatidaEnum.SAIDA);
        ponto2.setId(2);
        ponto2.setData_Hora_Batida(LocalDateTime.now().plusMinutes(1));
        Ponto ponto3 = new Ponto(usuario, TipoBatidaEnum.ENTRADA);
        ponto3.setId(3);
        ponto3.setData_Hora_Batida(LocalDateTime.now().plusMinutes(2));
        Ponto ponto4 = new Ponto(usuario, TipoBatidaEnum.SAIDA);
        ponto4.setId(4);
        ponto4.setData_Hora_Batida(LocalDateTime.now().plusMinutes(3));
        List<Ponto> pontos = Arrays.asList(ponto, ponto2, ponto3, ponto4);
        Mockito.when(pontoRepository.findAllByUsuario(Mockito.any(Usuario.class))).thenReturn(pontos);

        PontoSaidaDTO pontoSaidaDTO = pontoService.consultarPontosBatidos(1);

        Assertions.assertEquals(pontoSaidaDTO.getPontos().size(), 4);
        Assertions.assertEquals(pontoSaidaDTO.getPontos().get(ponto.getData_Hora_Batida()), TipoBatidaEnum.ENTRADA);
        Assertions.assertEquals(pontoSaidaDTO.getPontos().get(ponto2.getData_Hora_Batida()), TipoBatidaEnum.SAIDA);
        Assertions.assertEquals(pontoSaidaDTO.getPontos().get(ponto3.getData_Hora_Batida()), TipoBatidaEnum.ENTRADA);
        Assertions.assertEquals(pontoSaidaDTO.getPontos().get(ponto4.getData_Hora_Batida()), TipoBatidaEnum.SAIDA);
        Assertions.assertEquals(pontoSaidaDTO.getUsuario(), usuario);
        Assertions.assertEquals(pontoSaidaDTO.getHorasTrabalhadas().getMinute(), 2);
    }

}

