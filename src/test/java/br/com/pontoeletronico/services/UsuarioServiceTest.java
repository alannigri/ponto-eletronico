package br.com.pontoeletronico.services;

import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.models.dtos.UsuarioEntradaDTO;
import br.com.pontoeletronico.repositories.UsuarioRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.Optional;

@SpringBootTest
public class UsuarioServiceTest {

    @Autowired
    private UsuarioService usuarioService;

    @MockBean
    private UsuarioRepository usuarioRepository;

    Usuario usuario;

    @BeforeEach
    public void setUp() {
        usuario = new Usuario();
        usuario.setId(1);
        usuario.setCpf("22893285880");
        usuario.setNome_Completo("Alan Nigri");
        usuario.setEmail("alan@alan.com");
        usuario.setData_Cadastro(LocalDate.now());

    }

    @Test
    public void criarUmNovoUsuario() {
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);
        UsuarioEntradaDTO usuarioEntradaDTO = new UsuarioEntradaDTO();
        usuarioEntradaDTO.setNome_Completo(usuario.getNome_Completo());
        usuarioEntradaDTO.setCpf(usuario.getCpf());
        usuarioEntradaDTO.setEmail(usuario.getEmail());
        Usuario usuarioObjeto = usuarioService.criarUsuario(usuarioEntradaDTO);

        Assertions.assertEquals(usuarioObjeto.getId(), 1);
        Assertions.assertEquals(usuarioObjeto.getEmail(), usuario.getEmail());

    }

    @Test
    public void testarAtualizarUsuario() {
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        Mockito.when(usuarioRepository.save(Mockito.any(Usuario.class))).thenReturn(usuario);

        usuario.setNome_Completo("Thais");
        usuario.setEmail("thaisnigri@hotmail.com");
        usuario.setCpf("41227822871");

        UsuarioEntradaDTO usuarioTeste = new UsuarioEntradaDTO(usuario.getNome_Completo(), usuario.getCpf(),
                usuario.getEmail());
        Usuario usuarioObjeto = usuarioService.atualizarUsuario(1, usuarioTeste);

        Assertions.assertEquals(usuario, usuarioObjeto);
    }

    @Test
    public void testarAtualizarUmUsuarioQueNaoExiste() {
        Optional<Usuario> usuarioOptional = Optional.empty();
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        usuario.setNome_Completo("Thais");
        usuario.setEmail("thaisnigri@hotmail.com");
        usuario.setCpf("41227822871");

        UsuarioEntradaDTO usuarioTeste = new UsuarioEntradaDTO(usuario.getNome_Completo(), usuario.getCpf(),
                usuario.getEmail());

        Assertions.assertThrows(RuntimeException.class, () -> {
            usuarioService.atualizarUsuario(88, usuarioTeste);
        });
    }

    @Test
    public void testarConsultarUmUsuario() {
        Optional<Usuario> usuarioOptional = Optional.of(usuario);
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenReturn(usuarioOptional);
        Usuario usuarioObjeto = usuarioService.consultarUsuario(1);

        Assertions.assertEquals(usuario, usuarioObjeto);
    }

    @Test
    public void testarConsultarUmUsuarioQueNaoExiste() {
        Mockito.when(usuarioRepository.findById(Mockito.anyInt())).thenThrow(RuntimeException.class);
        Assertions.assertThrows(RuntimeException.class, () -> {
            usuarioService.consultarUsuario(2);
        });
    }

    @Test
    public void testarConsultarTodosUsuarios() {
        Iterable<Usuario> usuarios = Arrays.asList(usuario);
        Mockito.when(usuarioRepository.findAll()).thenReturn(usuarios);
        Iterable<Usuario> usuarioIterable = usuarioService.consultarTodosUsuarios();
        Assertions.assertEquals(usuarios, usuarioIterable);
    }
}
