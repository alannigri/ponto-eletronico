package br.com.pontoeletronico.controllers;

import br.com.pontoeletronico.enums.TipoBatidaEnum;
import br.com.pontoeletronico.models.Ponto;
import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.models.dtos.PontoSaidaDTO;
import br.com.pontoeletronico.models.dtos.UsuarioEntradaDTO;
import br.com.pontoeletronico.services.PontoService;
import br.com.pontoeletronico.services.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
@RequestMapping("/usuario")
public class UsuarioController {

    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private PontoService pontoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Usuario criarUsuario(@RequestBody @Valid UsuarioEntradaDTO usuarioEntradaDTO) {
        Usuario usuario = usuarioService.criarUsuario(usuarioEntradaDTO);
        return usuario;
    }

    @PutMapping("/{id}")
    public Usuario atualizarUsuario(@PathVariable int id, @RequestBody @Valid UsuarioEntradaDTO usuarioEntradaDTO) {
        try {
            Usuario usuarioObjeto = usuarioService.atualizarUsuario(id, usuarioEntradaDTO);
            return usuarioObjeto;
        } catch (RuntimeException exception) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, exception.getMessage());
        }
    }

    @PostMapping("{id}/ponto/{tipoBatida}")
    @ResponseStatus(HttpStatus.CREATED)
    public Ponto baterPonto(@PathVariable @Valid int id, @PathVariable TipoBatidaEnum tipoBatida) {
        try {
            Ponto ponto = pontoService.baterPonto(id, tipoBatida);
            return ponto;
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping
    public Iterable<Usuario> consultarUsuarios() {
        Iterable<Usuario> usuario = usuarioService.consultarTodosUsuarios();
        return usuario;
    }

    @GetMapping("{id}")
    public PontoSaidaDTO consultarPontosBatidos (@PathVariable @Valid int id){
        PontoSaidaDTO pontos= pontoService.consultarPontosBatidos(id);
        return pontos;
    }

}
