package br.com.pontoeletronico.enums;

public enum TipoBatidaEnum {
    ENTRADA ("ENTRADA"),
    SAIDA ("SAIDA");
//    ENTRADA2 ("ENTRADA"),
//    SAIDA2 ("SAIDA");

    private String nome_Tipo;

    TipoBatidaEnum(String nome_Tipo){
        this.nome_Tipo = nome_Tipo;
    }
}
