package br.com.pontoeletronico.repositories;

import br.com.pontoeletronico.models.Usuario;
import org.springframework.data.repository.CrudRepository;

public interface UsuarioRepository extends CrudRepository<Usuario, Integer> {

}
