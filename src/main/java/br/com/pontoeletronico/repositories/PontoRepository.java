package br.com.pontoeletronico.repositories;

import br.com.pontoeletronico.models.Ponto;
import br.com.pontoeletronico.models.Usuario;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PontoRepository extends CrudRepository<Ponto, Integer> {
    List<Ponto> findAllByUsuario (Usuario usuario);
}
