package br.com.pontoeletronico.services;

import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.models.dtos.UsuarioEntradaDTO;
import br.com.pontoeletronico.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Optional;

@Service
public class UsuarioService {

    @Autowired
    private UsuarioRepository usuarioRepository;

    public Usuario criarUsuario(UsuarioEntradaDTO usuarioEntradaDTO) {
        Usuario usuario = new Usuario(usuarioEntradaDTO.getNome_Completo(), usuarioEntradaDTO.getCpf(),
                usuarioEntradaDTO.getEmail());
        Usuario usuarioObjeto = usuarioRepository.save(usuario);
        return usuarioObjeto;
    }

    public Usuario atualizarUsuario(int id, UsuarioEntradaDTO usuarioEntradaDTO) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);
        if (usuarioOptional.isPresent()) {
            Usuario usuario = new Usuario(usuarioEntradaDTO.getNome_Completo(), usuarioEntradaDTO.getCpf(),
                    usuarioEntradaDTO.getEmail());
            usuario.setId(usuarioOptional.get().getId());
            usuario.setData_Cadastro(usuarioOptional.get().getData_Cadastro());
            Usuario usuarioObjeto = usuarioRepository.save(usuario);
            return usuarioObjeto;
        }
        throw new RuntimeException("Usuário não encontrado");
    }

    public Usuario consultarUsuario(int id) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id);
        if (usuarioOptional.isPresent()) {
            return usuarioOptional.get();
        }
        throw new RuntimeException("Usuário não encontrado");
    }

    public Iterable<Usuario> consultarTodosUsuarios() {
        Iterable<Usuario> usuarioIterable = usuarioRepository.findAll();
        return usuarioIterable;
    }
}
