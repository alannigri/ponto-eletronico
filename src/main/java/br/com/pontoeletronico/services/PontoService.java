package br.com.pontoeletronico.services;

import br.com.pontoeletronico.enums.TipoBatidaEnum;
import br.com.pontoeletronico.models.Ponto;
import br.com.pontoeletronico.models.Usuario;
import br.com.pontoeletronico.models.dtos.PontoSaidaDTO;
import br.com.pontoeletronico.repositories.PontoRepository;
import br.com.pontoeletronico.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

@Service
public class PontoService {

    @Autowired
    private PontoRepository pontoRepository;

    @Autowired
    private UsuarioRepository usuarioRepository;


    public Ponto baterPonto(int id_Usuario, TipoBatidaEnum tipoBatida) {
        Optional<Usuario> usuarioOptional = usuarioRepository.findById(id_Usuario);
        if (usuarioOptional.isPresent()) {
            List<Ponto> pontos = pontoRepository.findAllByUsuario(usuarioOptional.get());
            if (pontos.size() == 0 || pontos.size() == 2) {
                if (tipoBatida == TipoBatidaEnum.SAIDA) {
                    throw new RuntimeException("Bata a entrada antes da saída!");
                }
            }
            if (pontos.size() == 1 || pontos.size() == 3) {
                if (tipoBatida == TipoBatidaEnum.ENTRADA) {
                    throw new RuntimeException("Bata a saída antes da entrada!");
                }
            }
            if (pontos.size() == 4) {
                throw new RuntimeException("Usuario já bateu todos os pontos permitidos");
            }
            Ponto ponto = new Ponto(usuarioOptional.get(), tipoBatida);
            pontoRepository.save(ponto);
            return ponto;
        }
        throw new RuntimeException("Usuário não encontrado");
    }

    public PontoSaidaDTO consultarPontosBatidos(int id_Usuario) {
        Optional<Usuario> usuario = usuarioRepository.findById(id_Usuario);
        if (usuario.isPresent()) {
            Iterable<Ponto> pontos = pontoRepository.findAllByUsuario(usuario.get());
            TreeMap<LocalDateTime, TipoBatidaEnum> pontoList = new TreeMap<>();
            for (Ponto p : pontos) {
                pontoList.put(p.getData_Hora_Batida(), p.getTipo_Batida());
            }
            PontoSaidaDTO pontoSaidaDTO = new PontoSaidaDTO();
            pontoSaidaDTO.setUsuario(usuario.get());
            pontoSaidaDTO.setPontos(pontoList);
            pontoSaidaDTO.calcularHorasTrabalhadas();
            return pontoSaidaDTO;
        }
        throw new RuntimeException("Usuário não encontrado");
    }
}
