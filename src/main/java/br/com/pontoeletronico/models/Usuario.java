package br.com.pontoeletronico.models;

import org.hibernate.validator.constraints.br.CPF;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Entity
public class Usuario {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull
    private String nome_Completo;

    @NotNull
    @CPF
    private String cpf;

    @NotNull
    @Email
    private String email;

    private LocalDate data_Cadastro;



    public Usuario() {
    }

    public Usuario(String nome_Completo, String cpf, String email) {
        this.nome_Completo = nome_Completo;
        this.cpf = cpf;
        this.email = email;
        this.data_Cadastro = LocalDate.now();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome_Completo() {
        return nome_Completo;
    }

    public void setNome_Completo(String nome_Completo) {
        this.nome_Completo = nome_Completo;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public LocalDate getData_Cadastro() {
        return data_Cadastro;
    }

    public void setData_Cadastro(LocalDate data_Cadastro) {
        this.data_Cadastro = data_Cadastro;
    }


    @Override
    public String toString() {
        return "Usuario{" +
                "id=" + id +
                ", nome_Completo='" + nome_Completo + '\'' +
                ", cpf='" + cpf + '\'' +
                ", email='" + email + '\'' +
                ", data_Cadastro=" + data_Cadastro +
                '}';
    }
}
