package br.com.pontoeletronico.models.dtos;

import br.com.pontoeletronico.enums.TipoBatidaEnum;
import br.com.pontoeletronico.models.Usuario;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;

public class PontoSaidaDTO {

    private Usuario usuario;
    private TreeMap<LocalDateTime, TipoBatidaEnum> pontos;
    private LocalTime horasTrabalhadas;

    public PontoSaidaDTO() {
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public TreeMap<LocalDateTime, TipoBatidaEnum> getPontos() {
        return pontos;
    }

    public void setPontos(TreeMap<LocalDateTime, TipoBatidaEnum> pontos) {
        this.pontos = pontos;
    }

    public LocalTime getHorasTrabalhadas() {
        return horasTrabalhadas;
    }

    public void setHorasTrabalhadas(LocalTime horasTrabalhadas) {
        this.horasTrabalhadas = horasTrabalhadas;
    }

    public void calcularHorasTrabalhadas() {
        List<LocalDateTime> localDateTimes = new ArrayList(pontos.keySet());
        List<LocalTime> localTimes = new ArrayList<>();
        for(LocalDateTime l : localDateTimes){
            localTimes.add(l.toLocalTime());
        }
        if (pontos.isEmpty()) {
            setHorasTrabalhadas(LocalTime.parse("00:00:00.00"));
        } else if (pontos.size() == 1) {
            LocalTime horas = LocalTime.now().minusNanos(localTimes.get(0).toNanoOfDay());
            setHorasTrabalhadas(horas);
        } else if (pontos.size() == 2) {
            LocalTime horas = localTimes.get(1).minusNanos(localTimes.get(0).toNanoOfDay());
            setHorasTrabalhadas(horas);
        } else if (pontos.size() == 3) {
            LocalTime horas1 = localTimes.get(1).minusNanos(localTimes.get(0).toNanoOfDay());
            LocalTime horas2 = LocalTime.now().minusNanos(localTimes.get(0).toNanoOfDay());
            LocalTime total = horas1.plusHours(horas2.getHour())
                    .plusMinutes(horas2.getMinute());
            setHorasTrabalhadas(total);
        } else {
            LocalTime horas1 = localTimes.get(3).minusNanos(localTimes.get(2).toNanoOfDay());
            LocalTime horas2 = localTimes.get(1).minusNanos(localTimes.get(0).toNanoOfDay());
            LocalTime total = horas1.plusHours(horas2.getHour())
                    .plusMinutes(horas2.getMinute());
            setHorasTrabalhadas(total);
        }

    }
}