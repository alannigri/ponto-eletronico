package br.com.pontoeletronico.models.dtos;

public class UsuarioEntradaDTO {

    private String nome_Completo;
    private String cpf;
    private String email;

    public UsuarioEntradaDTO() {
    }

    public UsuarioEntradaDTO(String nome_Completo, String cpf, String email) {
        this.nome_Completo = nome_Completo;
        this.cpf = cpf;
        this.email = email;
    }

    public String getNome_Completo() {
        return nome_Completo;
    }

    public void setNome_Completo(String nome_Completo) {
        this.nome_Completo = nome_Completo;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
