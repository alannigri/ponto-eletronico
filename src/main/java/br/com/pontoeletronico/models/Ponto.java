package br.com.pontoeletronico.models;

import br.com.pontoeletronico.enums.TipoBatidaEnum;
import net.bytebuddy.dynamic.loading.InjectionClassLoader;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.time.LocalDateTime;
import java.time.LocalTime;

@Entity
public class Ponto {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @DateTimeFormat
    private LocalDateTime data_Hora_Batida;

    @NotNull
    private TipoBatidaEnum tipo_Batida;

    @ManyToOne(cascade = CascadeType.ALL)
    private Usuario usuario;

    public Ponto() {
    }

    public Ponto(Usuario usuario, TipoBatidaEnum tipo_Batida) {
        this.usuario = usuario;
        this.tipo_Batida = tipo_Batida;
        this.data_Hora_Batida = LocalDateTime.now();
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Usuario getUsuario() {
        return usuario;
    }

    public void setUsuario(Usuario usuario) {
        this.usuario = usuario;
    }

    public LocalDateTime getData_Hora_Batida() {
        return data_Hora_Batida;
    }

    public void setData_Hora_Batida(LocalDateTime data_Hora_Batida) {
        this.data_Hora_Batida = data_Hora_Batida;
    }

    public TipoBatidaEnum getTipo_Batida() {
        return tipo_Batida;
    }

    public void setTipo_Batida(TipoBatidaEnum tipo_Batida) {
        this.tipo_Batida = tipo_Batida;
    }

}
